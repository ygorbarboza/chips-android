package com.b2w.stretchview.chips_android.lib;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.lang.CharSequence;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mobile on 5/29/14.
 */
public class ChipsTextView extends TextView {

    private View mChipView;
    private TextView mTextView;
    private ArrayList<CharSequence> mItems = new ArrayList<CharSequence>();
    private OnChipClickListener mOnChipClickListener;

    public ChipsTextView(Context context) {
        super(context);
    }

    public ChipsTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ChipsTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setChipLayout(View chipView, int textviewId){
        mChipView = chipView;
        mTextView = (TextView) mChipView.findViewById(textviewId);
    }

    public void addChip(CharSequence text) {
        SpannableStringBuilder sb = new SpannableStringBuilder();
        final int position = mItems.size();
        mItems.add(text);

        requestViewLayout(mChipView);
        mTextView.setText(text);
        BitmapDrawable bd = (BitmapDrawable) convertViewToDrawable(mChipView);
        bd.setBounds(0, 0, bd.getIntrinsicWidth(), bd.getIntrinsicHeight());

        sb.append(text);
        sb.setSpan(new ImageSpan(bd), 0, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                System.out.println("teste");
                if(mOnChipClickListener != null){
                    mOnChipClickListener.onChipClick(position);
                }

            }
        }, 0, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        append(sb + " ");
    }

    public void removeChip(int position) {
        mItems.remove(position);

        setText("", TextView.BufferType.SPANNABLE);

        for(int i = 0; i < mItems.size(); i++){
            addChip(mItems.get(i));
        }
    }

    private Object convertViewToDrawable(View view) {
        int spec = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.measure(spec, spec);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        Bitmap b = Bitmap.createBitmap(view.getWidth(),
                view.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        view.draw(c);
        BitmapDrawable d = new BitmapDrawable(getResources(), b);

        return d;
    }

    public void requestViewLayout(View view){
        view.requestLayout();

        if(view instanceof ViewGroup){
            ViewGroup viewGroup = (ViewGroup) view;

            for(int i = 0; i < viewGroup.getChildCount(); i++){
                requestViewLayout(viewGroup.getChildAt(i));
            }
        }
    }

    public void setOnChipClickListener(OnChipClickListener onChipClickListener){
        mOnChipClickListener = onChipClickListener;
    }

    public interface OnChipClickListener{
        public void onChipClick(int position);
    }

}
