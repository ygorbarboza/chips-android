package com.b2w.chips.sample;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.b2w.stretchview.chips_android.lib.ChipsTextView;


public class MainActivity extends ActionBarActivity {

    private ChipsTextView mChipsTextView;
    private String[] mSchoolbag = { "Books", "Pens", "Pencils", "Notebooks" };
    private int mCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mChipsTextView = (ChipsTextView) findViewById(R.id.chips);
        mChipsTextView.setChipLayout(LayoutInflater.from(this).inflate(R.layout.item_chip, new RelativeLayout(this)), R.id.text);
        mChipsTextView.setOnChipClickListener(new ChipsTextView.OnChipClickListener() {
            @Override
            public void onChipClick(int position) {
                mChipsTextView.removeChip(position);
            }
        });
    }

    public void onAddChipClick(View view){
        mChipsTextView.addChip(mSchoolbag[mCount]);
        mCount = (mCount + 1) % mSchoolbag.length;
    }
}
